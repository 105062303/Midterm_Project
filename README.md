# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Shopping Website]
* Key functions (add/delete)
    1. [Membership Mechanism]
    2. [Firebase Hosting]
    3. [Database]
    4. [RWD]
    5. [Topic Key Function]
       [Product page]
       [Shopping pipeline]
       [User dashboard]
    
* Other functions (add/delete)
    1. [Third-Party Sign In]
    2. [Chrome Notification]
    3. [Use CSS Animation]
    4. [Security Report]
    5. [Other functions]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|  ----->  Firebase Hosting
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
  我將網站在firebase上hosting，網址:https://shopping-website-d7654.firebaseapp.com/   
[使用的Template](http://sc.chinaz.com/mobandemo.aspx?downloadid=26201525619780)  
  這次做的購物網站總共有15個頁面:   
  `1.最開始是首頁(index.html)，有自動播放的海報，每張圖片也都用了效果讓滑鼠點上去時會換另一張圖(other functions)`  
  `2.登入(sign2.html)的功能我用這邊[Sign in的Template](https://colorlib.com/wp/html5-and-css3-login-forms/)，可以新增帳號登入、google帳號、facebook帳號登入(Membership Mechanism、Third-Party Sign In)，另外按鈕有CSS Animation`  
  `3.購物車(checkout.html)可以一鍵清空，這邊的proceed to buy按鈕有用CSS Animation(震動效果):`   ![image](public/images/css.png)  
`proceed to buy按下去之後會跳出付款的彈窗(other functions):`  ![image](public/images/css1.png)  
`按下pay按鈕(含CSS Animation shadow效果)就能成功購買，購買後右下會出現Chrome Notification(前提是一開始進網站有允許通知):`   ![image](public/images/notifi.png)  
`這筆購買紀錄就會存進firebase database，包含時間、用戶信箱、總價錢、每項產品的名稱、還有各買了幾個，每筆資料都會依序被存進去 :`  ![image](public/images/data.png)   
  `4.歷史紀錄(history.html)，往下滑會看到show history按鈕(有CSS Animation)，點下後使用者僅能觀看前一次的購買紀錄，看到買了哪些商品、各買幾項、有哪些沒買過:`  ![image](public/images/his.png)  
  `5.商品列表(products.html)，這邊放了所有的8樣商品，可以在這裡把他們加入購物車，右列是分類、最熱門商品、還有一些營造音樂效果的hashtag`  
  `6~13.單項商品(single1.html ~ single8.html)，大致上跟商品列表很像，多了單項商品的介紹，每項單品下面都有推薦商品，讓商品間可以互相連結，右列跟商品列表一樣`  
  `14.contact.html，可以輸入想傳給我們的訊息，下面有定位好的google map(other functions)(localhost有但firebase hosting上貌似沒跑出來...):`   ![image](public/images/map.png)  
  `在縮小的網頁下、用手機試所有網頁都能順利操作，因此都含有RWD`

## Security Report (Optional)
  1.在firebase網頁上打開後是https協議, 即是安全的網站.  
  2.原先設置realtime database時，database.rules.json為default:  
// These rules require authentication  
`{  
  "rules": {  
    ".read": "auth != null",  
    ".write": "auth != null"  
  }  
}  `
如此任何人都能建立帳戶，然後成功讀取及存取資料，但資料並不會公開給認證用戶以外的人
